package src;


import java.util.Arrays;


import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.MovePilot;
import lejos.utility.Delay;
import lejos.utility.TimerListener;
import lejos.robotics.navigation.Move;
import lejos.robotics.navigation.Move.MoveType;


/**
 * Methodes de plus haut niveau qui definissent des comportements demandant l'interaction des perceptions et des actionneurs du robot;
 * Comporte 1 objet Actionneurs, 1 objet Capteurs, 1 objet PlanTable et un attribut pinceLibre, qui definit si les pinces possedent un palet.
 * @see Actionneurs 
 * @see Capteurs 
 * @see PlanTable
 * @author GroupeORI
 * @version 1.0
 */ 
public class Agent {
	protected Actionneurs actionneurs = new Actionneurs();
	protected Capteurs c = new Capteurs();
	protected boolean pinceLibre = true;
	protected PlanTable planTable;

	/**
	 * Retourne les actionneurs du robot.
	 * @return actionneurs, un attribut de type Actionneurs
	 * @see Actionneurs
	 */
	public Actionneurs getActionneurs() {
		return actionneurs;
	}
	/**
	 * Retourne les capteurs c du robot.
	 * @return c, un attribut de type Capteurs 
	 * @see Capteurs
	 */
	public Capteurs getC() {
		return c;
	}
	/**
	 * Retourne le boolean {@code pinceLibre} du robot.  
	 * @return un booleen,  true si la pince est libre et false si la pince est occupee
	 * 
	 */
	public boolean isPinceLibre() {
		return pinceLibre;
	}
	/** 
	 * Joue une melodie de victoire. Utilisée quand une action positive ce passe
	 */
	public static void melodieV() { //on garde paut etre pas, mais c'etait pour faire une petite musique de victoire/defaite personalisée 
		Sound.playTone(988,200,40);
		Sound.playTone(1318,200,60); 
	}
	/** 
	 * Joue une melodie de defaite. A utiliser quand quelque chose de negatif arrive 
	 */
	public static void melodieD() {
		Sound.playTone(587,200,40);
		Sound.playTone(138, 500,50);
	}

	/**
	 * Scanne la zone devant le robot sur 180 degres et s'oriente face a la cible la plus proche. <br>
	 * Si une cible est a moins de 55cm, le robot s'arrete directement.<br>
	 * Si une cible est a moins de 31cm elle est ignorée car c'est un mur ou un robot ennemi.<br>
	 * @return un boolean, true si il a localiser la cible, false sinon
	 * @see src.Capteurs#getDistance()
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 * @see #melodieV()
	 * @see src.Actionneurs#getCumulAngle()
	 */
	public boolean localiserCible() {
		System.out.print("LocaliserCible");
		float distanceValue = 0;
		c.getDistance();
		boolean trouve = false;
		float min = 3;
		int angleE=0;
		//c.getDistance();
		actionneurs.tourner(-75, 100, false);
		double tpsD = System.currentTimeMillis();
		double tpsF=tpsD ;
		actionneurs.tourner(150, 35,  true);

		while(actionneurs.pilot.isMoving() ) {
			//Button.LEDPattern(4);
			distanceValue = c.getDistance();
			if(distanceValue > 0.31 ) {
				if(distanceValue< min) {
					min =distanceValue;
					System.out.println("Minimum : "+min);
				}
				if(distanceValue< 0.55) {
					actionneurs.arreter();
					tpsF = System.currentTimeMillis();
					angleE= ((int) (((tpsF-tpsD)/1000)*150/5.5));
					System.out.println("angle parcouru : " + angleE);
					actionneurs.setCumulAngle(actionneurs.cumulAngle-150+angleE); //enleve la ditance non parcouru 
					actionneurs.tourner(10, 60, false);
					Sound.beepSequenceUp();
					melodieV();
					trouve = true;
					//Button.LEDPattern(0);
					return true;
				}
			}
			System.out.println( " Distance:  " + distanceValue);

			Delay.msDelay(100);
		}

		if(!trouve) {
			tpsD = System.currentTimeMillis();
			actionneurs.tourner(-150, 35, true);
		}


		while( actionneurs.pilot.isMoving() && !trouve){
			distanceValue =c.getDistance();

			if( distanceValue  <  min+0.02 && distanceValue>min-0.02 )  {
				actionneurs.arreter(); 
				tpsF = System.currentTimeMillis();
				angleE=(int)(((tpsF-tpsD)/1000)*150/5.7);
				System.out.println("angle parcouru : " + angleE);
				actionneurs.setCumulAngle(actionneurs.cumulAngle+angleE); //enleve la ditance non parcouru 
				actionneurs.tourner(-7, 60, true);	//pour corriger le decalage
				Sound.beepSequenceUp();
				melodieV();
				trouve = true;
				//Button.LEDPattern(0);
				return true;
			}
			System.out.println( " Distance: " + distanceValue);
			Delay.msDelay(100);
		}

		if(!trouve) {
			melodieD();
			//Button.LEDPattern(6);
			eviterMur();
			//Button.LEDPattern(0);
			return false;
		}
		//Button.LEDPattern(0);
		return false;
	}

	/**
	 * Prend le palet et verifie qu'il est bien dans ses pinces si il y a un risque d'avoir devier de la trajectoir initiale.<br>
	 * Le palet doit etre en face du robot.<br>
	 * La distance de la cible est definit au debut de la methode et ne doit pas varier au cours de son execution au risque de ne pas pouvoir l'attraper.<br>
	 * Affiche sur l'ecran du robot la distance de la cible au debut de l'execution.<br>
	 * 
	 * @return Un booleen, true s'il attrape un palet,  false sinon
	 * @see src.Actionneurs#avancer(double, double, boolean)
	 * @see src.Capteurs#getDistance()
	 * @see src.Actionneurs#ouvrirLaPince()
	 * @see src.Capteurs#getTactile()
	 * @see src.Actionneurs#arreter()
	 * @see src.Actionneurs#fermerLaPince()
	 * @see #melodieV()
	 * @see #melodieD()
	 */
	public boolean prendrePalet() {	

		float distanceValue = 0;
		distanceValue = c.getDistance();
		System.out.println( " Distance Cible : " + distanceValue);

		int d = ((int) (distanceValue*100))+5;
		if(d<33) {
			eviterMur();
			return false;
		}
		if(d>150)
			return false;
		actionneurs.ouvrirLaPince();
		actionneurs.avancer(d , 30, true);
		while(actionneurs.pilot.isMoving()) {
			if(c.getTactile() == 1) {
				actionneurs.arreter();
				actionneurs.fermerLaPince();
				pinceLibre = false;
				melodieV();
			}
		}
		if(pinceLibre) { 
			actionneurs.fermerLaPince();
			actionneurs.ouvrirLaPince();
			//actionneurs.reculer(5, 20, false);
			actionneurs.avancer(10 , 20, true);
			while(actionneurs.pilot.isMoving()) {
				//System.out.println( " Tactile : " + c.getTactile());
				if(c.getTactile() == 1) {
					actionneurs.arreter();
					melodieV();
					actionneurs.fermerLaPince();
					pinceLibre = false;
				}			//la on est sur qu'il a le palet
			}
			Delay.msDelay(100);
			if(pinceLibre) {
				melodieD();
				actionneurs.reculer(15, 30, false);
				actionneurs.fermerLaPince();
			}
		}
		return !pinceLibre;
	}


	/**
	 * Le robot evite le mur en faisant demi tour. <br>
	 * Si il a palet dans les pince alors il recule et appel la methode {@link #avancerVersCampAdverse()}.<br>
	 * @see #avancerVersCampAdverse()
	 * @see src.Actionneurs#reculer(double, double, boolean)
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 * @see Orientation
	 * @see src.Orientation#getOrientation(int)
	 **/
	public void eviterMur() {  
		if(pinceLibre==false) {	
			actionneurs.reculer(40, 35, false);
			avancerVersCampAdverse();
			return;
		}
		actionneurs.reculer(40, 35,false);
		if(Orientation.getOrientation(actionneurs.cumulAngle).equals(Orientation.NE) || Orientation.getOrientation(actionneurs.cumulAngle).equals(Orientation.SO))
			actionneurs.tourner(-120,80,false);
		else if(Orientation.getOrientation(actionneurs.cumulAngle).equals(Orientation.NO) || Orientation.getOrientation(actionneurs.cumulAngle).equals(Orientation.SE))
			actionneurs.tourner(120,80,false);
		else
			actionneurs.tourner(180, 80, false);
		actionneurs.avancer(20,30,false);
	}	

	/**
	 * Vas vers le camp adverse peu importe son orientation et deposer le palet quand il arrive sur la ligne blanche. 
	 * @return un boolean qui indique si le robot est bine dans le camp 
	 * @see Couleur
	 * @see Orientation
	 * @see #deposePaletCamps()
	 * @see #eviterMur()
	 * @see src.Orientation#getOrientation(int)
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 * @see src.Actionneurs#avancer(double, double, boolean)
	 * @see src.Actionneurs#arreter()
	 * @see src.Capteurs#getDistance()
	 * @see src.Capteurs#getCouleur()
	 */
	public boolean avancerVersCampAdverse() {
		Couleur c1=Couleur.GRIS;
		if(c.getDistance()<=0.2) { 
			eviterMur();
		}
		if(c.getCouleur().equals(Couleur.BLANC) && (Orientation.getOrientation(actionneurs.cumulAngle).equals(Orientation.S) || Orientation.getOrientation(actionneurs.cumulAngle).equals(Orientation.SE) || Orientation.getOrientation(actionneurs.cumulAngle).equals(Orientation.SO)))
			actionneurs.reculer(20, 35, false);
		double angleATourner = actionneurs.cumulAngle%360;
		actionneurs.tourner(-angleATourner,150,false);
		actionneurs.avancer(300, 30, true);
		System.out.println("J'avance vers campAdverse");
		while(actionneurs.pilot.isMoving()) {
			if(c.getCouleur().equals(Couleur.BLANC) && c1.equals(planTable.getLigneH1())) {
				System.out.println("Je suis dans mon camp !");
				actionneurs.arreter();
				actionneurs.reculer(35, 20, false);
				actionneurs.tourner(180, 100, false);
				actionneurs.setCumulAngle(10);
				return avancerVersCampAdverse();
			}
			if(c.getCouleur().equals(Couleur.BLANC)) {
				System.out.println("Je vois du "+c.getCouleur());
				actionneurs.arreter();
				if(!(c.getDistance()<=0.2)) {
					actionneurs.avancer(30, 25, true);
					while(actionneurs.pilot.isMoving())
						if(c.getDistance()<=0.20) {
							actionneurs.arreter();
							return true;
						}
				}
				return true;
			}
			if(c.getCouleur().equals(planTable.getLigneH1())) {
				System.out.println("Je suis proche de mon camp");
				c1=planTable.getLigneH1();
			}
			if(c.getCouleur().equals(planTable.getLigneV1())){
				c1=planTable.getLigneV1();
				System.out.println("Je remet a jour mon orientation");
				actionneurs.arreter();
				if(!c.getCouleur().equals(c1)) {
					actionneurs.reculer(25, 5, true);
					while(actionneurs.pilot.isMoving()) {
						if(c.getCouleur().equals(c1)) {
							actionneurs.arreter();
							System.out.println("Je retrouve la ligne");
						}
					}
				}
				avancerEnSuivantligne(14);
				actionneurs.avancer(250, 30, true);
				System.out.println("J'avance vers campAdverse");
				while(actionneurs.pilot.isMoving()) {
					if(c.getCouleur().equals(Couleur.BLANC)) {
						System.out.println("Je vois du "+c.getCouleur());
						actionneurs.arreter();
					}
				}
				if(!(c.getDistance()<=0.2)) {
					actionneurs.avancer(30, 25, true);
					while(actionneurs.pilot.isMoving())
						if(c.getDistance()<=0.20) {
							actionneurs.arreter();
							return true;
						}
				}
				return true;
			}
			if(c.getCouleur().equals(planTable.getLigneV3())){
				c1=planTable.getLigneV3();
				System.out.println("Je remet a jour mon orientation");
				actionneurs.arreter();
				if(!c.getCouleur().equals(c1)) {
					actionneurs.reculer(25, 5, true);
					while(actionneurs.pilot.isMoving()) {
						if(c.getCouleur().equals(c1)) {
							actionneurs.arreter();
							System.out.println("Je retrouve la ligne");
						}
					}
				}
				avancerEnSuivantligne(14);
				actionneurs.avancer(250, 30, true);
				System.out.println("J'avance vers campAdverse");
				while(actionneurs.pilot.isMoving()) {
					if(c.getCouleur().equals(Couleur.BLANC)) {
						System.out.println("Je vois du "+c.getCouleur());
						actionneurs.arreter();
					}
				}
				if(!(c.getDistance()<=0.2)) {
					actionneurs.avancer(30, 25, true);
					while(actionneurs.pilot.isMoving())
						if(c.getDistance()<=0.20) {
							actionneurs.arreter();
							return true;
						}
				}
				return true;
			}
		}
		if(c.getDistance()<0.10) {
			actionneurs.reculer(35, 35, false);
			actionneurs.tourner(180, 100, false);
			actionneurs.avancer(30, 30, false);
			return avancerVersCampAdverse();
		}
		return false;
	}

	/**
	 * Selectionne les actions a faire selon le type d'objet rencontre par le robot.<br>
	 * Si il est en face d'un mur, appel de {@link #eviterMur()}<br>
	 * Si il est en face d'un ennemi, appel de {@link #eviterEnnemi()}<br>
	 * Si il est en face d'un palet, appel de {@link #prendrePalet()} puis va le deposer dans le camps advers. <br>
	 * @param obj le type de l'objet rencontre
	 * @see Objet
	 * @see #eviterEnnemi()
	 * @see #eviterMur()
	 * @see #prendrePalet()
	 * @see #localiserCible()
	 * @see #avancerVersCampAdverse()
	 * @see #deposePaletCamps()
	 * @see src.Actionneurs#avancer(double, double, boolean)
	 */
	public void reactionFaceAObjet(Objet obj) {
		if(obj == Objet.R) {
			System.out.println("EviterEnnemi");
			eviterEnnemi();
		}
		else if(obj == Objet.M) {
			System.out.println("EviterMur");
			eviterMur();
		}
		else if (obj == Objet.P) {
			System.out.println("PrendrePalet");
			if(prendrePalet()) {
				avancerVersCampAdverse();
				deposePaletCamps();
				actionneurs.avancer(40, 30, false);
			}
			else
				eviterMur();
		}
	}

	/**
	 * Fait avancer le robot en suivant une ligne de couleur sur une distance donnee en centimetre<br>
	 * Il met a jour l'attribut orientation selon son orientation de depart et la ligne suivie.<br>
	 * @param distance la distance que doit parcourir le robot
	 * @see Couleur
	 * @see Actionneurs
	 * @see Capteurs
	 * @see Orientation
	 * @see PlanTable
	 * @see src.Capteurs#getCouleur()
	 * @see src.Actionneurs#avancer(double, double, boolean)
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 * @see src.Actionneurs#arreter()
	 * @see src.Actionneurs#reculer(double, double, boolean)
	 */
	public void avancerEnSuivantligne(double distance){
		Couleur couleur = c.getCouleur();
		int angle;
		int trouve;
		int nouveauCumul=0;
		double distanceRestante = distance;
		int nbCorrection=0;
		if(distance<7) {
			System.out.println("La distance est negative ou inferieure a 7 cm !");
			return;
		}
		if(couleur.equals(Couleur.GRIS) || couleur.equals(Couleur.AUTRE) || couleur.equals(Couleur.NOIR)) { //Si le robot n'est pas sur une ligne de couleur
			int ligne=0;
			System.out.println("Je cherche la ligne a droite");
			actionneurs.tourner(30, 45, true); //vitesse a determiner avec essais
			while(actionneurs.pilot.isMoving()) {
				if(c.getCouleur().equals(Couleur.JAUNE) || c.getCouleur().equals(Couleur.ROUGE)) {
					actionneurs.arreter();
					ligne=1;
					System.out.println("J'ai retrouve la ligne");
				}
			}
			if(ligne==0) {
				System.out.println("Je cherche la ligne a gauche");
				actionneurs.tourner(((-2)*30), 45, true); //vitesse a determiner avec essais
				while(actionneurs.pilot.isMoving()) {
					if(c.getCouleur().equals(Couleur.JAUNE) || c.getCouleur().equals(Couleur.ROUGE)) {
						actionneurs.arreter();
						trouve=1;
						System.out.println("J'ai retrouve la ligne");
					}
				}
			}
			if(ligne==0){
				actionneurs.tourner(20, 30, false);
				System.out.println("Le robot est sur la couleur : "+c.getCouleur());
				return;
			}
		}
		couleur=c.getCouleur();
		if(couleur.equals(Couleur.JAUNE) || couleur.equals(Couleur.ROUGE)) {
			while(distanceRestante>=7) {
				actionneurs.avancer(7, 50, false);
				distanceRestante -= 7;
				angle=20;
				trouve = 0;
				while (!c.getCouleur().equals(couleur) && trouve==0) { //Il a perdu la ligne de couleur et la recherche sur la gauche ou la droite
					System.out.println("Je cherche la ligne a droite");
					actionneurs.tourner(angle, 70, true); //vitesse a determiner avec essais
					while(actionneurs.pilot.isMoving()) {
						if(c.getCouleur().equals(couleur)) {
							actionneurs.arreter();
							trouve=1;
							System.out.println("J'ai retrouve la ligne");
						}
					}
					if(trouve==0) {
						System.out.println("Je cherche la ligne a gauche");
						actionneurs.tourner(((-2)*angle), 70, true); //vitesse a determiner avec essais
						while(actionneurs.pilot.isMoving()) {
							if(c.getCouleur().equals(couleur)) {
								actionneurs.arreter();
								trouve=1;
								System.out.println("J'ai retrouve la ligne");
							}
						}
					}
					if(trouve==0)
						actionneurs.tourner(angle, 70, false);
					if(trouve==1) {
						System.out.println("Je rentre dans la boucle pour modifier orientation");
						nbCorrection++;
						if(nbCorrection<2) {
							if(Orientation.getOrientation(actionneurs.cumulAngle).equals(Orientation.N) || Orientation.getOrientation(actionneurs.cumulAngle).equals(Orientation.NE) || Orientation.getOrientation(actionneurs.cumulAngle).equals(Orientation.NO))
								nouveauCumul = 0;
							else 
								nouveauCumul = 180;
							System.out.println("J'ai modifie l'orientation : "+actionneurs.cumulAngle);
						}
					}
					if(angle>80) { //Si il ne trouve vraiment pas la ligne il recule un peu
						System.out.println("Je ne retrouve pas la ligne");
						actionneurs.reculer(10, 100, true);
						while(actionneurs.pilot.isMoving()) {
							if(c.getCouleur().equals(couleur)) //Si le robot ne suit plus la ligne de couleur
								actionneurs.arreter();
						}
						distanceRestante+=3;
						if(c.getCouleur()!=couleur)
							System.out.println("Ligne perdue, il reste " + distanceRestante + " a parcourir");
					}
					angle+=20;
				}
			}
			actionneurs.setCumulAngle(nouveauCumul);
			actionneurs.avancer(distanceRestante, 50, false);
		}
	}

	/**
	 * Permet d'eviter un ennemi prealablement identifie par la methode avancerVers;
	 * Attend quelques temps si l'ennemi n'est plus visible ou est a une distance respectable.
	 * Si l'ennemi est encore trop proche il l'evite en changant de direction. 
	 * @see #eviterMur()
	 * @see src.Capteurs#presenceEnnemi()
	 * @see src.Capteurs#getDistance()
	 * @see src.Actionneurs#avancer(double, double, boolean)
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 * @see src.Actionneurs#arreter()
	 *
	 */
	public void eviterEnnemi() {
		actionneurs.arreter();
		Delay.msDelay(5000); 
		if (!c.presenceEnnemi() || (c.presenceEnnemi() && c.getDistance()>0.40)) {
			return;
		}
		actionneurs.tourner(40, 150, false);
		if (!c.presenceEnnemi()) {
			if(c.getDistance()<0.25)
				eviterMur();
			else {
				actionneurs.avancer(15, 30, false);
				actionneurs.tourner(-40, 150, false);
			}
		}
		else {
			actionneurs.tourner(-40, 150, false);
		}
	}

	/**
	 * Depose le palet. <br>
	 * Lorsqu'il a depose le palet, le robot se tourne vers l'air de jeu.
	 * @see Orientation
	 * @see src.Actionneurs#fermerLaPince()
	 * @see src.Actionneurs#ouvrirLaPince()
	 * @see src.Actionneurs#reculer(double, double, boolean)
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 */
	public void deposePaletCamps() {
		boolean pb=false;
		if(actionneurs.pinceOuverte==true) {
			actionneurs.fermerLaPince();
		}
		actionneurs.ouvrirLaPince();
		actionneurs.reculer(15, 35, true); //valeurs a changer
		Delay.msDelay(200);
		actionneurs.fermerLaPince();
		double angleATourner = 180 - (actionneurs.cumulAngle%180);
		if(angleATourner<0)
			angleATourner=-angleATourner;
		actionneurs.tourner(angleATourner, 50, false);
		/*int boucle=1;
		while(actionneurs.getPilot().isMoving()) {
			Delay.msDelay(700);
			double p1=c.getDistance();
			Delay.msDelay(700);
			double p2=c.getDistance();
			Delay.msDelay(700);
			double p3=c.getDistance();
			if(boucle>=3 && Math.abs(p1-p2)<0.07 && Math.abs(p2-p3)<0.07) {
				actionneurs.arreter();
				pb=true;
				break;
			}	
			boucle++;
		}
		if(pb) {
			actionneurs.reculer(20, 30, false);
			actionneurs.tourner(-190, 60, false);
			actionneurs.setCumulAngle(actionneurs.getCumulAngle()-((int)angleATourner-40));
			System.out.println(actionneurs.getCumulAngle());
		}*/
		pinceLibre=true;
	}

	/**
	 * Avance d'une certaine distance et d'identifie la cible <br>
	 * @param distanceInitiale la distance que le robot doit parcourir
	 * @param vitesse la vitesse a laquelle il doit parcourir la distance
	 * @param b un booleen qui permet d'executer la methode en meme temps qu'un autre partie du programme dans lequel elle est lancee.
	 * @return un Objet percu (Palet, Mur, Robot, Vide)
	 * @see Objet
	 * @see src.Capteurs#getDistance()
	 * @see src.Actionneurs#avancer(double, double, boolean)
	 * @see src.Actionneurs#arreter()
	 */
	public Objet avancerVers(double distanceInitiale, double vitesse, boolean b) {
		double perceptionDistance = distanceInitiale;
		double perceptionDistance2 = perceptionDistance;
		c.getDistance();

		if(c.presenceEnnemi()==true)
			return Objet.R;
		else {
			actionneurs.avancer(distanceInitiale, vitesse, b); 
			while(actionneurs.pilot.isMoving()) {
				double prochainePerception = c.getDistance();
				System.out.println(""+prochainePerception);
				/*if(c.presenceEnnemi()) {
					actionneurs.arreter();
					return Objet.R;
				}*/
				if(c.getDistance()<=0.30) {
					//Condition mur
					actionneurs.arreter();
					System.out.println("percoit mur");
					return Objet.M;
				}
				else if(perceptionDistance<0.35 && (prochainePerception>perceptionDistance || (prochainePerception==perceptionDistance && perceptionDistance==perceptionDistance2))) {
					//condition palet
					System.out.println("percoit un palet");
					return Objet.P;
				}
				else if (perceptionDistance>0.35 && (prochainePerception>perceptionDistance || c.getDistance()==0)) {
					//condition decalage
					actionneurs.arreter();
					System.out.println("C'est decale");
					return Objet.V;
				}
				perceptionDistance2 = perceptionDistance;
				perceptionDistance = prochainePerception;
			}
			System.out.println("Atteint la distance");
			return Objet.V; 
		}
	}
	
	/**
	 * Cherche une ligne d'une certain couleur. 
	 * @param action 			savoir si on cherche la ligne en avancant (1) ou en tournant (0) 
	 * @param distance		distance a parcurir ou angle a trourner 
	 * @param retour				true pour que le robot soit pil sur la ligne et false pour juste la trouver
	 * @param col 					couleur de la ligne voulue
	 * @return boolean, true si il trouve la ligne de la bonne couleur. False sinon
	 * @see Couleur 
	 * @see src.Capteurs#getCouleur()
	 * @see src.Actionneurs#avancer(double, double, boolean)
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 * @see src.Actionneurs#arreter()
	 */
	public boolean rechercheLigne(int action, double distance, boolean retour, Couleur col) {
		boolean trouve = false;
		if(action==0) { //S'il tourne
			actionneurs.tourner(distance, 50, true);
			while(actionneurs.pilot.isMoving()) {
				if(c.getCouleur().equals(col)) {
					actionneurs.arreter();
					System.out.println("J'ai trouve la ligne!");
					return true;
				}
			}
			if(retour && !c.getCouleur().equals(col)) {
				actionneurs.tourner(-(distance*2), 30, true);
				while(actionneurs.pilot.isMoving()) {
					if(c.getCouleur().equals(col)) {
						actionneurs.arreter();
						System.out.println("Je suis sur la ligne!");
						return true;
					}
				}
				actionneurs.tourner(distance, 50, false);
				System.out.println("Je n'ai pas trouve la ligne!");
			}
			else
				System.out.println("Je n'ai pas trouve la ligne!");
		}
		else { //Si il avance
			actionneurs.avancer(distance, 25, true);
			while(actionneurs.pilot.isMoving()) {
				if(c.getCouleur().equals(col)) {
					actionneurs.arreter();
					if(!retour) {
						System.out.println("J'ai trouve la ligne!");
						return true;
					}
					trouve=true;
				}
			}
			if(trouve && !c.getCouleur().equals(col)) {
				actionneurs.reculer(25, 3, true);
				while(actionneurs.pilot.isMoving()) {
					if(c.getCouleur().equals(col)) {
						actionneurs.arreter();
						System.out.println("Je suis sur la ligne!");
						return true;
					}
					if(c.getCouleur().equals(Couleur.BLANC)) {
						actionneurs.arreter();
						actionneurs.avancer(10, 30, false);
						break;
					}
				}
			}
			System.out.println("Je n'ai pas trouve la ligne!");
		}
		return false;
	}
	
	/* Cette methode a ete retiree car elle n'etais pas utilisee
	
	 * Fait reculer le robot jusqu'a ce qu'il trouve une ligne de couleur ou qu'il
	 * ait parcouru 60cm et le fait repartir dans une direction aleatoire; 
	 * Il renvoit la couleur de la ligne sur laquelle il s'arrete.
	 * @return la couleur de la ligne sur laquelle il s'est arrete
	 * @see Couleur
	 * @see src.Capteurs#getCouleur()
	 * @see src.Actionneurs#reculer(double, double, boolean)
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 * @see src.Actionneurs#arreter()

	 
	public Couleur remiseAZero() { 
		int nbAppel=1;
		boolean trouve = false;
		while(nbAppel<8 && !trouve) {
			actionneurs.reculer(15, 70, true);
			while(actionneurs.pilot.isMoving()) { //S'arrete s'il trouve une ligne de couleur
				if((c.getCouleur()==Couleur.ROUGE) || (c.getCouleur()==Couleur.BLANC) || (c.getCouleur()==Couleur.BLEU) || (c.getCouleur()==Couleur.VERT) || (c.getCouleur()==Couleur.JAUNE) || (c.getCouleur()==Couleur.NOIR)) {
					actionneurs.arreter();
					trouve=true;
					System.out.println("J'ai trouve une ligne de couleur : "+c.getCouleur());
				}	
			}
			if(!trouve) {
				//S'il ne percoit pas de ligne de couleur il recommence en zigzagant
				System.out.println("Je n'ai pas trouve la ligne, je recommence");
				actionneurs.tourner(((int) (Math.pow((-1), nbAppel)*30)), 150, false);
				if(nbAppel>1) {
					actionneurs.tourner(((int) (Math.pow((-1), nbAppel)*30)), 150, false);
				}
				nbAppel++;
			}
		}
		if(nbAppel>=7) {
			System.out.println("7 appels, je tourne");
			actionneurs.tourner((int) (Math.random()*361), 150, false);
		}
		System.out.println("Fin methode");
		//S'il percoit une ligne de couleur il tourne aleatoirement et renvoit la couleur percue
		return c.getCouleur();
	}

	 */

}