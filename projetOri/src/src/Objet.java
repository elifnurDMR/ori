package src;
/**
 * Type parametre definissant les differents types d'objets de l'espace de jeu<br>
 * Ils sont caracterises par une chaine de caractere nommant explicitement l'objet.<br>
 * @author GroupeORI
 * @version 1.0
 */
public enum Objet {
	M("Mur"),R("Robot Ennemi"),P("Palet"),V("Vide");
	/**
	 * La chaine de caractere associe a l'objet.
	 */
	String nomObjet;
	
	/**
	 * Le constructeur qui initialise un objet par son nom.
	 * @param nomObjet la chaine de caractere identifiant un objet
	 */
	private Objet(String nomObjet) {
		this.nomObjet=nomObjet;
	}
	
	/**
	 * Retourne la valeur de type {@link Objet} correspondant a la chaine de caractere en parametre.
	 * @param nom le nom de l'objet en chaine de caractere
	 * @return un attribut de type objet correspondant et V si aucun identifier
	 */
	public static Objet identifierObjet(String nom) {
		switch(nom) {
			case "Mur" : return M;
			case "Robot Ennemi" : return R;
			case "Palet" : return P;
			default : return V;
		}
	}
}