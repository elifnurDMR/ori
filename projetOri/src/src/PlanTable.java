package src;

import lejos.hardware.Button;

/**
 * Plan de la table de jeu<br>
 * Elle definit les couleurs des lignes verticales et horizontales en fonction du camp de depart<br>
 * Les couleurs des lignes horizontales et verticales du centre sont toujours les memes, elles sont noires<br>
 * La classe permet aussi d'obtenir les couleurs des lignes horizontales et verticales exterieures.<br>
 * @author GroupeORI
 * @version 1.0
 *
 */
public class PlanTable {
	/**
	 * La ligne de couleur la plus proche du camp, parallele a la ligne de demarcation du camp.
	 * @see Couleur
	 */
	private Couleur ligneH1;
	/**
	 * La ligne de couleur la plus lointaine du camp, parallele a la ligne de demarcation du camp.
	 * @see Couleur
	 */
	private Couleur ligneH3;
	/**
	 * La ligne de couleur la plus a gauche du robot lorsqu'il est en position de depart.
	 * @see Couleur
	 */
	private Couleur ligneV1;
	/**
	 * La ligne de couleur la plus a droite du robot lorsqu'il est en position de depart.
	 * @see Couleur
	 */
	private Couleur ligneV3;
	
	/** 
	 * Definit la configuration du plan en fonction de la valeur du bouton sur lequel l'utilisateur aura appuye.<br>
	 * Bouton du haut = 1 <br>
	 * Bouton du bas = 2 <br>
	 * Les lignes H sont presentees de la plus proche du camp de depart a la plus lointaine.<br>
	 * Les lignes V sont definies de gauche a droite en face du robot.<br>
	 * @see Couleur
	 * @see Strategie
	 * @see PlanTable
	 */
	public PlanTable() {

		int valBouton; // entier representant la valeur du bouton appuye
		System.out.println("Appuyer haut si 1ere ligne = bleue \n Appuyer bas si 1ere ligne = verte");
		valBouton = Button.waitForAnyPress();
		while(valBouton!= Button.ID_UP && valBouton!=Button.ID_DOWN) {
			System.out.println("Pas le bon bouton appuye, recommencez");
			valBouton = Button.waitForAnyPress();
		}
		if (valBouton == Button.ID_UP) { //pour mettre ID_UP il faut implements lejos.hardware.keys 
			ligneH1 = Couleur.BLEU;
			ligneH3 = Couleur.VERT;
			ligneV1 = Couleur.JAUNE;
			ligneV3 = Couleur.ROUGE;
			System.out.println("PlanTable initalise bleu");
		}
		else if(valBouton == Button.ID_DOWN){ //m�me pb que UP
			ligneH1 = Couleur.VERT;
			ligneH3 = Couleur.BLEU;
			ligneV1 = Couleur.ROUGE;
			ligneV3 = Couleur.JAUNE;
			System.out.println("PlanTable initalise vert");
		}
	}
	
	/**
	 * Retourne la valeur du type {@link Couleur} correspondant a la ligne horizontale la plus proche du camp du robot.
	 * @return ligneH1 la couleur de la ligne la plus proche du camp et parallele a celui ci
	 * @see Couleur
	 */
	public Couleur getLigneH1() {
		return ligneH1;
	}
	
	/**
	 * Retourne la valeur du type {@link Couleur} correspondant a la ligne horizontale la plus lointaine du camp du robot.
	 * @return ligneH1 la couleur de la ligne la plus lointaine du camp et parallele a celui ci
	 * @see Couleur
	 */
	public Couleur getLigneH3() {
		return ligneH3;
	}
	
	/**
	 * Retourne la valeur du type {@link Couleur} correspondant a la ligne verticale la plus a gauche de la position initiale du robot.
	 * @return ligneH1 la couleur de la ligne la plus a gauche du robot en position initiale
	 * @see Couleur
	 */
	public Couleur getLigneV1() {
		return ligneV1;
	}
	
	/**
	 * Retourne la valeur du type {@link Couleur} correspondant a la ligne verticale la plus a droite de la position initiale du robot.
	 * @return ligneH1 la couleur de la ligne la plus a droite du robot en position initiale
	 * @see Couleur
	 */
	public Couleur getLigneV3() {
		return ligneV3;
	}
}