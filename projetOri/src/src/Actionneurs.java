package src;
import lejos.hardware.motor.Motor;
import lejos.robotics.chassis.*;
import lejos.robotics.navigation.MovePilot;
import lejos.utility.Delay;
/**
 * Les actionneurs du robot.<br>
 * Il y en a 2 principaux, la pince et les roues du moteur rassemblees en 1 chassis dirige par un pilot;<br>
 * Il contient une variable pinceOuverte pour verifier l'etat d'ouverture de la pince;<br>
 * Il definit la valeur de la vitesse maximum de deplacement du robot et du temps necessaire pour ouvrir la pince.<br>
 * @author GroupeORI
 * @version 1.0
 */
public class Actionneurs {
	private Wheel roueD = WheeledChassis.modelWheel(Motor.B, 5.6).offset(6.9);
	private Wheel roueG = WheeledChassis.modelWheel(Motor.C, 5.6).offset(-6.9);
	private Chassis chassis = new WheeledChassis(new Wheel[] {roueD, roueG}, WheeledChassis.TYPE_DIFFERENTIAL);
	protected MovePilot pilot = new MovePilot(chassis);
	public MovePilot getPilot() {
		return pilot;
	}

	private final static int TPS_OUVERTURE_PINCE = 1200;
	private final static int TPS_FERMETURE_PINCE = 1205;
	protected boolean pinceOuverte = false;
	public boolean isPinceOuverte() {
		return pinceOuverte;
	}
	/**
	 * Donne le cumul d'angle en cours 
	 * @return Un entier qui correspond au cumul d'angle
	 */
	public int getCumulAngle() {
		return cumulAngle;
	}
	/**
	 * variable qui sert de memoire d'orientation
	 */
	protected int cumulAngle = 0;

	public Actionneurs() {}
	
	/**
	 * Fait avancer le robot d'une certaine distance donnee en cm a une vitesse donnee
	 * @param distance	la distance en cm sur laquelle on veut faire avancer le robot
	 * @param vitesse 		une valeur en mm/s
	 * @param c 				Si true : le programme continu d'etre execute. Si false : pause pensant le temps du mouvement  
	 * @see MovePilot
	 */
	public void avancer(double distance, double vitesse, boolean c){	 
		if(distance < 0) distance = -distance;
		pilot.setLinearSpeed(vitesse);  
		pilot.setLinearAcceleration(vitesse-0.2*vitesse);		//permet de modifier la fluidite 
		pilot.travel(distance, c);   
	}
	
	/**
	 * Methode qui permet au robot de reculer d'une certaine distance donnee en cm a une vitesse donnee 
	 * @param distance 	la distance en cm sur laquelle on veut faire reculer le robot
	 * @param vitesse		une valeur en mm/s
	 * @param c					Si true : le programme continu d'etre execute. Si false : pause pensant le temps du mouvement  
	 * @see MovePilot
	 */
	public void reculer(double distance, double vitesse, boolean c) { 
		if(distance < 0) distance = -distance;
		pilot.setLinearSpeed(vitesse);  
		pilot.setLinearAcceleration(vitesse - 0.2*vitesse);		//permet de modifier la fluidite 
		pilot.travel(-distance, c);  
	}
	
	/**
	 * Methode permettant au robot de tourner d'un certain angle a une vitesse fixee<br>
	 * met a jours {@code cumulAngle}<br>
	 * 
	 * @param angle			Angle en degre 
	 * @param vitesse		une valeur en mm/s
	 * @param c					Si true : le programme continu d'etre execute. Si false : pause pensant le temps du mouvement 
	 * @see MovePilot
	 * 
	 */
	public void tourner(double angle, double vitesse, boolean c ) {	
		pilot.setAngularSpeed(vitesse);
		pilot.setAngularAcceleration(vitesse - 10);
		double angleC=angle;		//permet de rectifier le decalage que le robot a lorsqu'il tourne
		if(angle>0)
			angleC=angle-(angle*0.115);
		else
			angleC=angle+((-angle)*0.115);
		pilot.rotate(angleC, c);
		cumulAngle+=(int) angle;
		System.out.println("Orientation : " + cumulAngle);
	}
	
	/**
	 * Arrete les mouvements
	 * @see MovePilot
	 */
	public void arreter() {		
		pilot.stop();
	}
		
	/**
	 * Ouvre la pince. <br>
	 * met a jour <code>pinceOuverte</code>
	 * @see Motor
	 */
	public void ouvrirLaPince() {
		if (pinceOuverte == false) {
			Motor.A.setSpeed(1000);		
			Motor.A.forward();
			Delay.msDelay(TPS_OUVERTURE_PINCE);
			Motor.A.stop();
			pinceOuverte=true;
		}
	}
	
	/**
	 * Ferme la pince. <br>
	 * met a jour <code>pinceOuverte</code>
	 * @see Motor
	 */
	public void fermerLaPince() {
		if (pinceOuverte == true) {
			Motor.A.setSpeed(1000);		
			Motor.A.backward();
			Delay.msDelay(TPS_FERMETURE_PINCE);
			Motor.A.stop(); 
			pinceOuverte=false;
		}
		
	}
	/**
	 * Modifie l'attribut <code>cumulAngle</code>
	 * @param cumulAngle	nouvelle valeur de {@code cumulAngle}
	 */
	public void setCumulAngle(int cumulAngle) {
		this.cumulAngle = cumulAngle;
	}
}
