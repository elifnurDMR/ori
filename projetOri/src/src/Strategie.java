package src;
import lejos.hardware.Button;
import lejos.hardware.motor.Motor;
import lejos.utility.Delay;
import src.*;

/**
 * La strategie du robot, c'est ici que ce trouvent les instructions pour prendre le 1er palet le plus rapidement possible et marquer les 5 points ainsi que les 3 suivants d'une ligne exterieure. <br>
 * Puis l'association de methode qui creent l'autonomie du robot.
 * 
 * @author GroupeORI
 * @version 1.0
 *
 */
public class Strategie extends Agent{
	/**
	 * Consite a attraper et ammener le palet de la ligne du centre le plus rapidment possibe dans le camp adverse. <br>
	 * Si le coter droit a ete appuyer dans la phase d'initialisation il va tourner a droite apres avoir pris le palet et inversement si c'est le bouton gauche <br>
	 * @param bouton instruction donner par l'utilisateur quand il a appuyer sur les boutons au lancement du robot
	 * @see src.Agent#deposePaletCamps()
	 * @see src.Agent#prendrePalet()
	 * @see src.Actionneurs#avancer(double, double, boolean)
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 * @see src.Actionneurs#arreter()
	 * @see src.Actionneurs#setCumulAngle(int)
	 * @see src.Capteurs#getCouleur()
	 * @see src.Capteurs#getDistance()
	 */
	public void premierCoup(int bouton) {
		if(bouton == Button.ID_RIGHT) {
			prendrePalet();
			actionneurs.tourner(25, 150, false); 
			actionneurs.avancer(175, 300, false);
			actionneurs.tourner(-12, 150, false);
			actionneurs.setCumulAngle(0);
			actionneurs.avancer(40, 30, true);
			while(actionneurs.pilot.isMoving()) {
				if(c.getCouleur().equals(Couleur.BLANC)) {
					actionneurs.arreter();
					if(!(c.getDistance()<=0.2)) {
						actionneurs.avancer(30, 25, true);
						while(actionneurs.pilot.isMoving())
							if(c.getDistance()<=0.2)
								actionneurs.arreter();
					}
					deposePaletCamps();
					actionneurs.avancer(10, 30, false);
				}
			}
		}
		else if(bouton == Button.ID_LEFT) {
			prendrePalet();
			actionneurs.tourner(-12, 150, false); 
			actionneurs.avancer(100, 300, false);
			actionneurs.tourner(32, 150, false);
			actionneurs.setCumulAngle(0);
			actionneurs.avancer(100, 100, true);
			while(actionneurs.pilot.isMoving()) {
				if(c.getCouleur().equals(Couleur.BLANC)) {
					actionneurs.arreter();
					if(!(c.getDistance()<=0.2)) {
						actionneurs.avancer(30, 25, true);
						while(actionneurs.pilot.isMoving())
							if(c.getDistance()<=0.2)
								actionneurs.arreter();
					}
					deposePaletCamps();
					actionneurs.avancer(10, 30, false);
				}
			}
		}
		System.out.println("1 palet pris");
	}

	/**
	 * Determine ou tourne le robot selon la position initiale de l'adversaire<br>
	 * Adversaire positionne a  droite = appuyer a gauche (ID = 16)<br>
	 * Adversaire positionne a gauche = appuyer a droite (ID = 8)<br>
	 * @return Un entier correspondant a la valeur du bouton appuyer
	 */
	public static int directionPremierCoup() {
		System.out.println("Ou tourne le robot ? (D/G)");
		int bouton = Button.waitForAnyPress();
		while(bouton!=Button.ID_LEFT && bouton!=Button.ID_RIGHT) {
			System.out.println("Tu n'as pas appuye sur le bon bouton : G ou D");
			bouton = Button.waitForAnyPress();
		}
		return bouton;
	}
	/**
	 * Va chercher le premier palet d'une ligne exterieur, apres avoir deposer un palet dans le capms adverse.
	 * @param direction indique vers quel ligne exterieur aller
	 * @see src.Agent#rechercheLigne(int, double, boolean, Couleur)
	 * @see src.Agent#avancerEnSuivantligne(double)
	 * @see src.Agent#deposePaletCamps()
	 * @see src.Actionneurs#ouvrirLaPince()
	 * @see src.Actionneurs#fermerLaPince()
	 * @see src.Actionneurs#avancer(double, double, boolean)
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 * @see src.Actionneurs#arreter()
	 * @see src.Actionneurs#setCumulAngle(int)
	 * @see src.Agent#melodieV()
	 * @see src.Agent#melodieD()
	 */
	public void premierPaletLigne(int direction) {
		int angle = 0;
		if(direction==Button.ID_LEFT) {
			angle=70;
			actionneurs.tourner(angle, 190, false);
			//c.getCouleur(); //instanciation couleur
			rechercheLigne(1,60,false,planTable.getLigneV1());
			if(rechercheLigne(0,-(angle+15),false,planTable.getLigneV1())) {
				avancerEnSuivantligne(7);
			}
			else
				rechercheLigne(0,-(angle-40),false,planTable.getLigneV1());
			actionneurs.ouvrirLaPince();
			actionneurs.avancer(40, 30, true);
			while(actionneurs.pilot.isMoving()) {
				if(c.getTactile() == 1) {
					actionneurs.arreter();
					actionneurs.fermerLaPince();
					pinceLibre = false;
					melodieV();
				}
			}
			if(pinceLibre) {
				melodieD();
				actionneurs.fermerLaPince();
			}
			actionneurs.tourner(180, 150, false);
			actionneurs.setCumulAngle(0);
		}
		else {
			angle = -70;
			actionneurs.tourner(angle, 100, false);
			//c.getCouleur(); //instanciation couleur
			rechercheLigne(1,60,false,planTable.getLigneV3());
			if(rechercheLigne(0,-(angle-15),false,planTable.getLigneV3())) {
				avancerEnSuivantligne(7);
			}
			else
				rechercheLigne(0,-(angle+40),false,planTable.getLigneV3());
			actionneurs.ouvrirLaPince();
			actionneurs.avancer(40, 30, true);
			while(actionneurs.pilot.isMoving()) {
				if(c.getTactile() == 1) {
					actionneurs.arreter();
					actionneurs.fermerLaPince();
					pinceLibre = false;
					melodieV();
				}
			}
			if(pinceLibre) {
				melodieD();
				actionneurs.fermerLaPince();
			}
			actionneurs.tourner(190, 150, false);
			actionneurs.setCumulAngle(0);
		}
		rechercheLigne(1,90,false,Couleur.BLANC);
		if(!(c.getDistance()<=0.2)) {
			actionneurs.avancer(30, 25, true);
			while(actionneurs.pilot.isMoving())
				if(c.getDistance()<=0.20)
					actionneurs.arreter();
		}
		deposePaletCamps();
		actionneurs.avancer(10, 30, false);
		System.out.println("2eme palet pris");
		//CampAdverse();
	}
	/**
	 * Va chercher le deuxieme palet d'une ligne exterieur
	 * @param direction indique vers quel ligne exterieur aller
	 * @see src.Agent#rechercheLigne(int, double, boolean, Couleur)
	 * @see src.Agent#avancerEnSuivantligne(double)
	 * @see src.Agent#deposePaletCamps()
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 * @see src.Actionneurs#ouvrirLaPince()
	 * @see src.Actionneurs#fermerLaPince()
	 * @see src.Actionneurs#avancer(double, double, boolean)
	 * @see src.Actionneurs#tourner(double, double, boolean)
	 * @see src.Actionneurs#arreter()
	 * @see src.Actionneurs#setCumulAngle(int)
	 * @see src.Agent#melodieV()
	 * @see src.Agent#melodieD()
	 */
	public void deuxiemePaletLigne(int direction) {
		if(direction==Button.ID_LEFT) {
			if(!rechercheLigne(1,10,true,planTable.getLigneV1())) {
				if(!rechercheLigne(0,75,true,planTable.getLigneV1())) {
					if(rechercheLigne(1,50,true,planTable.getLigneV1())) {
						avancerEnSuivantligne(13);
						prendrePalet();
						actionneurs.tourner(180, 50, false);
						actionneurs.setCumulAngle(0);
					}
					else if(c.getDistance()<0.5) {
						System.out.println("Je tente quand m�me");
						prendrePalet();
						actionneurs.tourner(180, 50, false);
						actionneurs.setCumulAngle(0);
					}
				}
				else {
					avancerEnSuivantligne(25);
					prendrePalet();
					actionneurs.tourner(180, 50, false);
					actionneurs.setCumulAngle(0);
				}
			}
			else {
				avancerEnSuivantligne(25);
				prendrePalet();
				actionneurs.tourner(180, 50, false);
				actionneurs.setCumulAngle(0);
			}
		}
		else {
			if(!rechercheLigne(1,10,true,planTable.getLigneV3())) {
				if(!rechercheLigne(0,-75,true,planTable.getLigneV3())) {
					if(rechercheLigne(1,50,true,planTable.getLigneV3())) {
						avancerEnSuivantligne(13);
						prendrePalet();
						actionneurs.tourner(-180, 50, false);
						actionneurs.setCumulAngle(0);
					}
					else if(c.getDistance()<0.5) {
						System.out.println("Je tente quand m�me");
						prendrePalet();
						actionneurs.tourner(-180, 50, false);
						actionneurs.setCumulAngle(0);
					}
				}
				else {
					avancerEnSuivantligne(25);
					prendrePalet();
					actionneurs.tourner(-180, 50, false);
					actionneurs.setCumulAngle(0);
				}
			}
			else {
				avancerEnSuivantligne(25);
				prendrePalet();
				actionneurs.tourner(-180, 50, false);
				actionneurs.setCumulAngle(0);
			}
		}
		avancerVersCampAdverse();
		if(!(c.getDistance()<=0.2)) {
			actionneurs.avancer(30, 25, true);
			while(actionneurs.pilot.isMoving())
				if(c.getDistance()<=0.2)
					actionneurs.arreter();
		}
		deposePaletCamps();
		actionneurs.avancer(10, 30, false);
		System.out.println("3eme palet pris");
	}

	/**
	 * Verifie la presence du troisieme palet sur la ligne.  <br> 
	 * Suit la ligne jusqu'a trouver un palet ou etre sur qu'il n'y a pas de palet.
	 * @return true si le palet est la, false sinon
	 * @param direction indique sur quel ligne exterieur le robot ce trouve
	 * @see #avancerEnSuivantligne(double)
	 * @see #rechercheLigne(int, double, boolean, Couleur)
	 * @see src.PlanTable#getLigneV1()
	 * @see src.PlanTable#getLigneV3()
	 * @see src.Capteurs#getDistance()
	 */
	public boolean presenceTroisiemePaletLigne(int direction) {
		if(direction==Button.ID_LEFT) {
			if(!rechercheLigne(1,20,true,planTable.getLigneV1())) {
				if(!rechercheLigne(0,75,true,planTable.getLigneV1())) {
					if(rechercheLigne(1,60,true,planTable.getLigneV1()))
						avancerEnSuivantligne(40);
				}
				else
					avancerEnSuivantligne(70);
			}
			else
				avancerEnSuivantligne(70);
		}
		else {
			if(!rechercheLigne(1,20,true,planTable.getLigneV3())) {
				if(!rechercheLigne(0,-75,true,planTable.getLigneV3())) {
					if(rechercheLigne(1,60,true,planTable.getLigneV3()))
						avancerEnSuivantligne(40);
				}
				else
					avancerEnSuivantligne(70);
			}
			else
				avancerEnSuivantligne(70);
		}
		return (c.getDistance()>0.25 && c.getDistance()<1);
	}

	public static void main(String[]args) {
		Strategie s = new Strategie();
		//instance de tout 
		s.c.presenceEnnemi();
		s.c.getDistance();
		s.c.getCouleur();
		s.c.getTactile();

		//initialiser le plan de table
		s.planTable = new PlanTable();

		int boutonDirection =Strategie.directionPremierCoup();
		//fait le premier coup
		s.premierCoup(boutonDirection);
		s.premierPaletLigne(boutonDirection);
		s.deuxiemePaletLigne(boutonDirection);
		/*boolean p3 = s.presenceTroisiemePaletLigne(boutonDirection);			//mis en commentaires car le matche ne durait pas 5min et cette etape devenait contreproductive
		System.out.println(p3);
		if(p3) {		//si il y a toujours le 3eme palet on le prends 
			s.prendrePalet();
			s.avancerVersCampAdverse();
			s.actionneurs.avancer(30, 25, true);
			while(s.actionneurs.pilot.isMoving())
				if(s.c.getDistance()<=0.15)
					s.actionneurs.arreter();
			s.deposePaletCamps();
			if(boutonDirection == Button.ID_LEFT) 	//j'ai peut etre inverser les + et les - pour l'orientations 
				s.actionneurs.tourner(-30 ,50, false);	 //s'oriente de facon optimale face au jeu
			else if(boutonDirection == Button.ID_RIGHT)
				s.actionneurs.tourner(30, 50, false);
			s.actionneurs.avancer(40,50,false);
		}*/	//sinon on commence l'autonomie
		if(boutonDirection == Button.ID_LEFT ) {
			s.actionneurs.tourner(-70 ,50, false);	//s'oriente dos au mur
			s.actionneurs.avancer(30, 30, false);
		}
		else if(boutonDirection == Button.ID_RIGHT) {
			s.actionneurs.tourner(70, 50, false);
			s.actionneurs.avancer(30, 30, false);
		}

		//
		//	suite d'evenements qui rendent le robot autonome
		//

		Motor.A.setSpeed(1000);		
		Motor.A.backward();
		Delay.msDelay(100);
		Motor.A.stop(); 
		while(true) {  	// tant qu'on appuie pas sur echape
			if(s.localiserCible() && s.pinceLibre) {	//si c'est true c'est qu'il doit poursuivre ses actions. Si c'est false c'est qu'il a deja un palet ou qu'il a rien trouver 
				Objet o = s.avancerVers((s.c.getDistance()*100)+10, 12, true);		//il vas vers sa cible 
				s.reactionFaceAObjet(o);								//l'idenifie et agit en consequences
			}
			else if(!s.pinceLibre)	{			//si il a un palet ou qu'il a ete interronpu 
				s.avancerVersCampAdverse();
			}
		}
	}
}
