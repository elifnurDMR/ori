package src;

/**
 * Type parametre qui definit les valeurs prises pour definir l'orientation du robot.<br>
 * Les valeurs prises correspondent aux points cardinaux usuels.<br>
 * La difference reside dans le fait que les points Nord, Sud, Est et Ouest ne sont definis que par une seule valeur, les autres points etant defini par une plage de valeurs.<br>
 * @author GroupeORI
 * @version 1.0
 */
public enum Orientation {
	N, S, E, O, NE, NO, SE, SO;
	
	/**
	 * Retourne l'orientation du robot en fonction de la valeur d'angle i donnee en parametre.<br>
	 * A partir du centre de la table de jeu, delimite par l'intersection des deux lignes noires, on definit le nord comme etant le milieu du camp adverse.<br>
	 * @param i la valeur de l'angle duquel le robot a tourne.
	 * @return un attribut de type {@link Orientation}, donnant l'orientation du robot sur la table de jeu.
	 * @see Orientation
	 * @see src.Actionneurs#cumulAngle
	 */
	public static Orientation getOrientation(int i) {
		if(i%360 == 0)
			return N;
		else if(i%360 == 180 || i%360 == -180)
			return S;
		else if(i%360 == 90 || i%360 == -240)
			return E;
		else if(i%360 == 240 || i%360 == -90)
			return O;
		else if ((i%360>0 && i%360<90) || (i%360>-360 && i%360<-240))
			return NE;
		else if((i%360>90 && i%360<180) || (i%360>-240 && i%360<-180))
			return SE;
		else if((i%360>180 && i%360<240) || (i%360>-180 && i%360<-90))
			return SO;
		return NO;
	}
}