package src;
/**
 * Les differents etats dans lequel le robot peut se trouver;
 * Ils sont associes a une valeur numerique par simplicite.
 * @author GroupeORI
 * @version 1.0
 *
 */
public enum Etat {
	RECHERCHE_PREMIER_PALET(0), DEPOSER_PREMIER_PALET(1), RECHERCHE_DEUXIEME_PALET(2), DEPOSER_DEUXIEME_PALET(3), RECHERCHER_PALET(4), DEPOSER_PALET(5), BLOQUE(6), PERDU(7);
	private int numero;
	
	private Etat(int i) {
		numero=i;
	}
	
	public int getNumero() {
		return numero;
	}
	
	/**
	 * Retourne la valeur Etat correspondant a l'entier i en parametre.
	 * @param i un entier
	 * @return un attribut de type Etat correspondant a i
	 * @see Strategie
	 */
	public Etat identifierEtat(int i) {
		switch(i) {
		case 0 : return RECHERCHE_PREMIER_PALET;
		case 1 : return DEPOSER_PREMIER_PALET;
		case 2 : return RECHERCHE_DEUXIEME_PALET;
		case 3 : return DEPOSER_DEUXIEME_PALET;
		case 4 : return RECHERCHER_PALET;
		case 5 : return DEPOSER_PALET;
		case 6 : return BLOQUE;
		default : return PERDU;
		}
	}
}