package src;


import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.Port;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.hardware.sensor.SensorMode;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

/**
 * Les capteurs du robot<br>
 * Il y en a 3, le capteur ultrason, couleur et tactile<br>
 * les 2 premieres  variables correspondent aux valeurs renvoyees par les capteurs pour la distance et le capteur tactile<br>
 * Une derniere prend un objet de type Couleur et identifie la couleur captee par le capteur couleur. <br>
 * @author GroupeORI
 * @version 1.0
 *
 */
public class Capteurs {
	private float distance; 
	private Couleur couleur;
	private int tactile;
	private EV3ColorSensor capteurCouleur;
	private EV3TouchSensor capteurTactile;
	private EV3UltrasonicSensor capteurUltrason;

	/**
	 * Retourne une flottant qui correspond a la distance entre le robot et un objet, mesuree par le capteur ultrason
	 * @return La valeur de la distance percue en metre.
	 */
	public float getDistance() {
		if(capteurUltrason == null) {
			capteurUltrason = new EV3UltrasonicSensor(SensorPort.S4);
			/*try {
				capteurUltrason = new EV3UltrasonicSensor(SensorPort.S4);
			}
			catch(IllegalArgumentException e) {
				System.out.println("Probleme invalidSensorMode");
				getDistance();
			}*/
		}
		final SampleProvider sp = capteurUltrason.getDistanceMode();
		distance = 0;
		float [] sample = new float[sp.sampleSize()];
		sp.fetchSample(sample, 0);
		distance = sample[0];	
		return distance;
	}

	/**
	 * Retourne un entier qui est la valeur renvoyee par le capteur tactile, selon l'etat du capteur (enfonce ou non)
	 * @return La valeur renvoyee par le capteur tactile. Elle est egale a -1, 0 ou 1.
	 */
	public int getTactile() {
		if(capteurTactile == null)
			capteurTactile = new EV3TouchSensor(SensorPort.S3);
		final SampleProvider sp = capteurTactile.getTouchMode();
		float [] sample = new float[sp.sampleSize()];
		sp.fetchSample(sample, 0);
		tactile = (int) sample[0];
		return tactile;
	}

	/**
	 * Retourne une instance de type Couleur qui est la couleur percue par la capteur couleur.
	 * @return Le nom de la couleur percue.
	 * @see Couleur
	 */
	public Couleur getCouleur() {
		if(capteurCouleur == null)
			capteurCouleur = new EV3ColorSensor(SensorPort.S2);
		couleur = Couleur.identifierCouleur(capteurCouleur.getColorID());
		return couleur;
	}

	/**
	 * Retourne un booleen informant si un autre robot est en face du robot, detectee par le mode presence du capteur ultrason.
	 * @return Un booleen, true s'il y a un robot, false s'il n'y en a pas.
	 */
	public boolean presenceEnnemi() {
		if(capteurUltrason == null)
			capteurUltrason = new EV3UltrasonicSensor(SensorPort.S4);
		SampleProvider mesureEnnemi = capteurUltrason.getListenMode();
		float [] tabEnnemi = new float[1];
		mesureEnnemi.fetchSample(tabEnnemi, 0);
		if(tabEnnemi[0] == 1)
			return true;
		else
			return false;
	}
}
