package src;
/**
 * Type parametre qui definit les valeurs que peut prendre une couleur<br>
 * Il est caracterise par une valeur numerique.<br>
 * @author GroupeORI
 * @version 1.0
 *
 */
public enum Couleur {
	ROUGE(0), VERT(1), BLEU(2), JAUNE(3), BLANC(6), NOIR(7), GRIS(-1), AUTRE(13); //ATTENTION GRIS == -1
	//compareTo fonctionne sur l'ordre de definition des constantes
	
	/**
	 * L'entier correspondant a la valeur de chaque couleur detecte par le capteur couleur.
	 * @see Capteurs
	 */
	private int valeur;
	
	/**
	 * Constructeur qui initialise un objet de type Couleur par sa valeur v.
	 * @param v l'entier correspondant a une couleur, capte par le capteur couleur du robot.
	 * @see Capteurs
	 */
	private Couleur(int v) {
		valeur=v;
	}
	
	//values() donne un tableau avec toutes les valeurs possibles du type Couleur
	
	/**
	 * Retourne la valeur de la couleur sur laquelle est appliquee la methode.
	 * @return valeur, l'entier correpondant a la valeur de la couleur.
	 * @see Capteurs
	 */
	public int getValeur() {
		return valeur;
	}
	
	/**
	 * Retourne le nom de la couleur, definie par le type Couleur correspondant a l'entier v passe en parametre.
	 * @param v un entier
	 * @return Couleur le nom de la couleur associe a l'entier v
	 * @see Capteurs
	 */
	public static Couleur identifierCouleur(int v) { //ATTENTION A BIEN REMODIFIER LES COULEURS : GRIS ==-1
		switch(v) {
		case 0 : return ROUGE;
		case 1 : return VERT;
		case 2 : return BLEU;
		case 3 : return JAUNE;
		case 6 : return BLANC;
		case 7 : return NOIR;
		case -1 : return GRIS;
		default : return AUTRE;
		}
	}
	
	/**
	 * Retourne la valeur de la couleur passee en parametre
	 * @param c un attribut de type Couleur
	 * @return un entier qui correspond a la couleur en parametre
	 * @see Couleur
	 */
	public static int valeur(Couleur c) { 
		switch(c) {
		case ROUGE : return 0;
		case VERT : return 1;
		case BLEU : return 2;
		case JAUNE : return 3;
		case BLANC : return 6;
		case NOIR : return 7;
		case GRIS : return -1;
		default : return 13;
		}
	}
}