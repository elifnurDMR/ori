package test;


import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.utility.Delay;
import src.Couleur;
import src.PlanTable;
import src.Strategie;

public class TestStrategie {
	Strategie s = new Strategie();
	PlanTable planTable = new PlanTable();

	private void testInitialiserPlanTable() {
		System.out.println("Appuyer bas si vert \n haut si bleu");
		//Appuyer sur bas
		planTable = new PlanTable();
		System.out.println("ligne H1 = Vert: " + (planTable.getLigneH1()==Couleur.VERT));
		System.out.println("ligne H3 = Bleu: " + (planTable.getLigneH3()==Couleur.BLEU));
		System.out.println("ligne V1 : "+ (planTable.getLigneV1()==Couleur.ROUGE));
		System.out.println("ligne V3 : "+ (planTable.getLigneV3()==Couleur.JAUNE));
		//Appuyer sur haut
		PlanTable planTable2 = new PlanTable();
		System.out.println("ligne H1 = Bleu: " + (planTable2.getLigneH1()==Couleur.BLEU));
		System.out.println("ligne H3 = Vert: " + (planTable2.getLigneH3()==Couleur.VERT));
		System.out.println("ligne V1 : "+ (planTable2.getLigneV1()==Couleur.JAUNE));
		System.out.println("ligne V3 : "+ (planTable2.getLigneV3()==Couleur.ROUGE));	
	}
	
	private void testDirectionPremierCoup() {
		System.out.println("Appuyer droite");
		int bouton = s.directionPremierCoup();
		if(bouton==8) {
			System.out.println("tourne � doite");
		}
		else {
			System.out.println("il y a un pb, valeur entree = " + bouton);
		}
		
		System.out.println("Appuyer  gauche");
		bouton = s.directionPremierCoup();
		if(bouton==16) {
			System.out.println("tourne � gauche");
		}
		else {
			System.out.println("il y a un pb, valeur entree = " + bouton);
		}
	}
	
	private void testPremierCoup() {
		//tester en appuyant � gauche, � droite, sur un autre bouton peu importe lequel
		System.out.println("Appuyer droit pour aller a droite sinon gauche");
		s.premierCoup(Button.waitForAnyPress());
	}

	public void testPremierPaletLigne() {
		//situe au camps adverse entre la ligne de milieu et V3 face au sud
		System.out.println("Au depart : droite ou gauche ?");
		s.premierPaletLigne(Button.waitForAnyPress());
		Sound.beep();
		Delay.msDelay(10000);
		//idem mais entre milieu et v1
		/*s.premierPaletLigne(Button.ID_LEFT);
		Sound.beep();*/
	}
	
	public void testDeuxiemePaletLigne() {
		s.deuxiemePaletLigne(Button.ID_RIGHT);
	}
	
	public void testPresenceTroisiemePaletLigne() {
		System.out.println(s.presenceTroisiemePaletLigne(Button.waitForAnyPress()));
		Sound.beep();
		System.out.println(s.presenceTroisiemePaletLigne(Button.waitForAnyPress()));
	}
	public static void main(String[]args) {
		TestStrategie t = new TestStrategie();
		//t.testInitialiserPlanTable();
		//t.testDirectionPremierCoup();
		t.testPremierCoup();
		t.testPremierPaletLigne();
		t.testDeuxiemePaletLigne();
		//t.testPresenceTroisiemePaletLigne();
		
	}
}



