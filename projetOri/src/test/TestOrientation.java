package test;

import lejos.hardware.Sound;
import lejos.utility.Delay;
import src.*;

public class TestOrientation {
	static Actionneurs a  = new Actionneurs();;
	double angle;
	


	public void testGetOrientation() {
		Orientation res;
		//Le robot est orient� face au Nord
		angle = 0;
		res=Orientation.N;
		if(Orientation.getOrientation(a.getCumulAngle())==res)
			System.out.println("Bonne orientation");
		else
			System.out.println("Mauvaise orientation : " + Orientation.getOrientation(a.getCumulAngle()));
		Delay.msDelay(5000);
		Sound.beep();
		//On oriente le robot face au Sud-Est
		angle = 190;
		a.tourner(angle, 100, false);
		res=Orientation.SO;
		if(Orientation.getOrientation(a.getCumulAngle())==res)
			System.out.println("Bonne orientation");
		else
			System.out.println("Mauvaise orientation : " + Orientation.getOrientation(a.getCumulAngle()));
		Delay.msDelay(5000);
		Sound.beep();
		//On oriente le robot � l'Ouest
		angle =-190-90;
		a.tourner(angle, 100, false);
		res = Orientation.O;
		if(Orientation.getOrientation(a.getCumulAngle())==res)
			System.out.println("Bonne orientation");
		else
			System.out.println("Mauvaise orientation : " + Orientation.getOrientation(a.getCumulAngle()));
		Delay.msDelay(5000);
		Sound.beep();
		angle = 90;
		a.tourner(angle, 100, false);
		a.tourner(-45, 100, false);
		res = Orientation.NO;
		if(Orientation.getOrientation(a.getCumulAngle())==res)
			System.out.println("Bonne orientation");
		else
			System.out.println("Mauvaise orientation : " + Orientation.getOrientation(a.getCumulAngle()));
	}
	
	public static void main(String[]args) {
		TestOrientation o = new TestOrientation();
		o.testGetOrientation();
		
	}

}
