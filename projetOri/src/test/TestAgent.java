package test;

import lejos.hardware.Sound;
import lejos.utility.Delay;
import src.*;

public class TestAgent {
	Agent a = new Agent();



	public void testLocaliserCible() {
		//faire le teste avec un mur en face; avec un palet plus proche que le mur; sans rien autour ; avec un mur a moins de 25cm d'un coter et un palet de l'autre; avec un mur d'un coté et rien de l'autre
		a.localiserCible();
	}

	
	public void testPrendrePalet() {
		// placer le palet devant le robot a 30cm, 50cm, 70cm
		System.out.println(a.prendrePalet());
	}

	
	public void testEviterMur() {
		// Mettre le robot � plus de 25cm d'un mur
		a.getActionneurs().setCumulAngle(200);
		a.eviterMur();
		// Si il tourne � 180 degre c'est bon 
		// Refaire le test en mettant le robot � moins de 25cm d'un mur 
		// Il doit d'abort reculer puis tourner de 180 degre
	}

	
	public void testAvancerVersCampAdverse() {
		// Faire le test en le mettant pas loins d'une ligne mais en changant de ligne. 
		// Faire le test en le mettant sur une ligne.
		System.out.print(""+a.avancerVersCampAdverse());
		// Il doit bouger afin d'aller jusqu'au camps adverse puis il lache le palet. 
	}

	
	public void testReactionFaceAObjet() {  
		//Mettre � palet devant
		a.reactionFaceAObjet(Objet.P);
		// Il doit prendre le palet 
		a.reactionFaceAObjet(Objet.M);
		// Il doit faire demis tour
		Sound.beep();
		Delay.msDelay(5000);
		a.reactionFaceAObjet(Objet.R);
		// Il doit eviter le robot
		
	}

		public void testAvancerEnSuivantligne() {
		double d;
		//Teste en avancant de 60 cm
		d = 70;
		a.avancerEnSuivantligne(d);
		Delay.msDelay(5000);	
	}
	

	public void testAvancerEnSuivantligneException() {
		//Teste avec une valeur de distance n�gative
		double t = -10;
		a.avancerEnSuivantligne(t);
		Delay.msDelay(5000);
		
		//Teste avec le robot qui n'est pas sur une ligne de couleur
		a.avancerEnSuivantligne(10);
	}


	public void testEviterEnnemi() {
		double angleI;
		//Teste la methode face a un ennemi a 35cm de lui
		//Le robot doit tourner sur la droite, avancer et se remettre en position initiale
		angleI=0;
		a.getActionneurs().avancer(30, 100, true);
		Delay.msDelay(1000);
		a.eviterEnnemi();
		Sound.beep();
		Delay.msDelay(10000);
		//Teste la methode en position initiale face a un ennemi a 45 cm de lui
		//Le robot doit s'arreter, attendre 5 secondes et renvoyer fin de la methode
		a.getActionneurs().avancer(30, 100, true);
		Delay.msDelay(1000);
		a.eviterEnnemi();
		Sound.beep();
		Delay.msDelay(10000);
		//Teste la methode en position initiale face a un ennemi, a moins de 30 cm de lui et a 20 cm d'un mur sur la droite
		//L'ennemi se deplace en direction du mur ou on place un autre ennemi proche du mur une fois que le robot est proche du mur
		//Le robot doit tourner sur la droite, puis revenir dans l'orientation initiale
		//Il renvoie fin de la methode
		angleI=0;
		a.getActionneurs().avancer(30, 100, true);
		Delay.msDelay(1000);
		a.eviterEnnemi();
		Sound.beep();
	}


//	public void testRemiseAZero() {
//		//Le robot s'arrete sur une ligne de couleur peu importe laquelle (mais positionn�e � plus de 20 cm de sa position de d�part si possible) 
//		//et reculer d'au max 60 cm
//		//Tous les 20 cm le robot doit tourner de 40 sur la droite ou la gauche pour zigzager
//		//A la fin, le robot doit tourner d'un angle al�atoire
//		//Il retourne la couleur qu'il percoit
//		Couleur res;
//		res = a.remiseAZero();
//		System.out.println(res);
//		Sound.beep();
//	}


	public void testDeposePaletCamps() {
		a.getC().getDistance();
			//le cas ou la pince est fermee
		//a.avancerVersCampAdverse();
		a.deposePaletCamps();

		/* Commentaire sur le test
		 * le robot doit avancer de 50 .. et on suppose qu'il arrive devant le camp adverse
		 * Le robot a ouvert la pince ?
		 * le robot a recule de 100.. ?
		 * Est-ce que 100 .. c'est trop (imaginer que l'on est sur la table) ?
		 * le robot a ferme la pince ?
		 * le robot a tourne ?
		 * Est-ce que l'angle est suffisant pour ne pas qu'il soit face � un mur ?
		 */
		/*Delay.msDelay(100);
		a.getActionneurs().ouvrirLaPince();
		if(a.getActionneurs().isPinceOuverte()==true) {
			//le cas ou la pince est ouverte
			a.deposePaletCamps();
		}*/
		//est-ce qu'il realise les memes actions ?
	}


	public void testAvancerVers() {
		//premier cas ou il doit s'arreter quand la distance est parcouru 
		/*System.out.println("le robot doit s'arreter quand la distance est parcourue");		
		a.avancerVers(10, 25, true);
		Sound.beep();
		Delay.msDelay(10000);

		//2eme cas le robot pas a 50 cm du mur
		System.out.println("le robot doit s'arreter a cause d'un mur");
		a.avancerVers(70, 25, true);
		//est-ce que le robot s'arrete afin de ne pas percuter le mur
		Sound.beep();
		Delay.msDelay(10000);

		//3eme cas lorsque l'on est decalee mettre un palet a 100 cm
		System.out.println("le robot doit s'arreter car il est decale");
		a.avancerVers(150, 70, true);
		//est-ce que le robot s'arrete car il est decale
		Sound.beep();
		Delay.msDelay(15000);*/
		//a.localiserCible();
		//4eme cas le robot s'arrete pour un palet mettre un palet a 40 cm
		//System.out.println("le robot doit s'arreter car il a un palet");
		a.avancerVers(100, 20, true);
		
	}

	public void rechercheLigne() {
		/*if(a.rechercheLigne(0,30,false,Couleur.JAUNE))
			System.out.println("C'est top !");
		else
			System.out.println("Bouh...");
		Delay.msDelay(5000);
		
		if(a.rechercheLigne(0,75,true,Couleur.JAUNE))
			System.out.println("C'est top !");
		else
			System.out.println("Bouh...");
		Delay.msDelay(5000);*/
		a.getC().getCouleur();
		a.rechercheLigne(1,75,false,Couleur.JAUNE);
		
		Delay.msDelay(5000);
		
		/*if(a.rechercheLigne(0,75,true,Couleur.BLANC))
			System.out.println("C'est top !");
		else
			System.out.println("Bouh...");*/
	}
		
	public static void main (String[] args) {
		TestAgent t  = new TestAgent();
		//t.testLocaliserCible();
		//t.testRemiseAZero();
		//t.testRemiseAZeroException();
		//t.testAvancerEnSuivantligne();
		//t.testAvancerVersCampAdverse();
		//t.testRemiseAZero();
		//t.testEviterMur();
		//t.testEviterEnnemi();
		//t.testAvancerVers();
		t.testDeposePaletCamps();
		//t.testPrendrePalet();
		//t.testReactionFaceAObjet();
		//t.rechercheLigne();
	}

}
