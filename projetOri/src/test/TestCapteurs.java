package test;


import lejos.hardware.Sound;
import lejos.utility.Delay;
import src.*;

public class TestCapteurs {
private static Capteurs c = new Capteurs();



	public void testGetDistance() {
		//On accepte une valeur avec une marge d'erreur de 10%
		//Place le robot � une distance de 8cm
		System.out.println(" Distance : " + c.getDistance());
		Sound.beep();
		Delay.msDelay(5000); //Tu as 5 secondes pour le changer de place
		//Place le robot � une distance de 23cm
		System.out.println(" Distance : " + c.getDistance());
		Sound.beep();
		Delay.msDelay(5000); //Tu as 5 secondes pour le changer de place
		//Place le robot juste en face d'un mur
		System.out.println(" Distance : " + c.getDistance());
		Sound.beep();
		Delay.msDelay(5000);
		//placer le robot a une distance de 60cm
		System.out.println(" Distance : " + c.getDistance());
		Sound.beep();
	}


	public void testGetTactile() {
		for(int i = 0; i<20; i++) {
			System.out.println( " Tactile : " + c.getTactile());
			Delay.msDelay(500);}
//		//Rien ne doit toucher le capteur
//		System.out.println( c.getTactile());
//		Delay.msDelay(10000);
//		//Le capteur doit �tre enfonc�
//		System.out.println(c.getTactile());
	}


	public void testGetCouleur() {
		//Deux choses sont test�es : � la fois la valeur de la couleur renvoy�e + identifierCouleur
		//Mettre le robot sur une ligne de couleur rouge
		System.out.println(c.getCouleur());
		Delay.msDelay(8000);
		//Mettre le robot sur une ligne de couleur blanche
		System.out.println(c.getCouleur());
		Delay.msDelay(10000);
		//Mettre le robot sur 2 couleurs en m�me temps
		System.out.println(c.getCouleur());
	} 
	

	public void testPresenceEnnemi() {
		Capteurs c = new Capteurs();
		if(c.presenceEnnemi());
		Sound.playTone(988,200,40);
	}
	
	public static void main (String[]args ) {
		TestCapteurs c = new TestCapteurs();
		//c.testGetTactile();
		c.testGetDistance();
		//c.testGetCouleur();
		//c.testPresenceEnnemi();
	}
}
