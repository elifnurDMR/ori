package test;


import lejos.hardware.Button;
import lejos.utility.Delay;
import src.PlanTable;

public class TestPlanTable {
	PlanTable p = new PlanTable();

	public void testPlanTable() {
		//Appuyer sur le bouton du haut
		new PlanTable();
		Delay.msDelay(5000);
		//Appuyer sur le bouton de gauche ou de droite puis le bouton du bas
		new PlanTable();
	}


	public void testGetLigneH1() {
		System.out.print(p.getLigneH1());
	}


	public void testGetLigneH3() {
		System.out.print(p.getLigneH3());
	}


	public void testGetLigneV1() {
		System.out.print(p.getLigneV1());
	}


	public void testGetLigneV3() {
		System.out.print(p.getLigneV3());
	}
	
	public static void main (String[]args) {
		TestPlanTable t = new TestPlanTable();
		t.testPlanTable();
		//t.testGetLigneH1();
		//t.testGetLigneH3();
		//t.testGetLigneV1();
		//t.testGetLigneV3();
		
	}

}
