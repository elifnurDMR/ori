package test;

import lejos.hardware.Sound;
import lejos.utility.Delay;
import src.Actionneurs;
import src.Capteurs;

public class TestActionneurs {
	Actionneurs a = new Actionneurs(); // car je viens de me rendre compte qu'en faisant un main �a va poser probl�me
	Capteurs c = new Capteurs();


	public void testAvancer() {
		/*a.avancer(20, 20, false);		//avance d'une courte distance lentement 
		Delay.msDelay(1000);
		
		a.avancer(-20, 50, false);		//avance d'une courte distance avec une erreure dans les parametres 
		Delay.msDelay(1000);
		
		a.avancer(20, 70, false);		//avance d'une courte distance rapidement 
		Delay.msDelay(1000);*/
		
		Delay.msDelay(5000);
		c.getDistance();
		a.avancer(150, 30, true);
		while(a.getPilot().isMoving())
			System.out.println("Distance"+c.getDistance());
		Sound.beep();
		//Delay.msDelay(5000);
		//a.avancer(50, 30, false);//avancer sur une longue ditance

	}


	public void testReculer() {
		/*a.reculer(20, 20, false);		//recule d'une courte distance
		Delay.msDelay(1000);
		
		a.reculer(-20, 50, false);		//recule d'une courte distance avec une erreure dans les parametres 
		Delay.msDelay(1000);
		
		a.reculer(20, 70, false);		//recule d'une courte distance rapidement 
		Delay.msDelay(1000);*/
		
		//Delay.msDelay(5000);
		a.reculer(40, 30, false);		//recule sur une longue ditance
		a.tourner(180, 40, false);

	}

	public void testTourner() {
		double tpsD = System.currentTimeMillis();
		c.getDistance();
		a.tourner(180, 40, true); // Le robot tourne de 90 degres
		while(a.getPilot().isMoving()) {
			System.out.println(c.getDistance());
			Delay.msDelay(700);
			System.out.println(c.getDistance());
			Delay.msDelay(700);
			System.out.println(c.getDistance());
			Delay.msDelay(700);
		}
		/*Delay.msDelay(700);
		double tpsF = System.currentTimeMillis();
		Sound.beep();
		System.out.println("Milisecondes : "+(tpsF-tpsD)/1000);
		
		Delay.msDelay(3000);
		tpsD = System.currentTimeMillis();
		a.tourner(-180, 40, false); // Le robot revient a son point de depart
		tpsF = System.currentTimeMillis();
		Sound.beep();
		System.out.println("Milisecondes : "+(tpsF-tpsD)/1000); 
		Delay.msDelay(7000);
		
		tpsD = System.currentTimeMillis();
		a.tourner(180, 40, false);
		tpsF = System.currentTimeMillis();
		Sound.beep();
		System.out.println("Milisecondes : "+(tpsF-tpsD)/1000); 
		
		Delay.msDelay(3000);
		tpsD = System.currentTimeMillis();
		a.tourner(180, 40, false);
		tpsF = System.currentTimeMillis();
		Sound.beep();
		System.out.println("Milisecondes : "+(tpsF-tpsD)/1000); 
		Delay.msDelay(7000);*/
	}


		public void testArreter() { // Il peut etre mis n'importe o� 
			a.avancer(50, 70, true);
			a.arreter();
			a.tourner(180, 70, true);
			a.arreter();
			// Il doit rien se passer
	}

	/**
	 * On suppose que la pince est fermee au debut
	 * on test le cas ou la pince est fermee et on veut l'ouvrir
	 * puis le cas ou la pince est ouverte et on appelle ouvrirLaPince
	 */
	public void testOuvrirLaPince() {

		System.out.print("la pince est fermee");
		a.ouvrirLaPince();
		//Commentaire, est-ce qu'il a ouvert la pince ?
		
		Delay.msDelay(30);
		System.out.print("la pince est ouverte");
		a.fermerLaPince();
		//Commentaire, est-ce qu'il n'a rien fait ?

	}


	/**
	 * On suppose que la pince est ouverte au debut
	 * on test le cas ou la pince est fermee et on veut l'ouvrir
	 * puis le cas ou la pince est ouverte et on appelle ouvrirLaPince
	 */
	public void testFermerLaPince() {
		System.out.print("la pince est ouverte");
		a.fermerLaPince();
		//Commentaire, est-ce qu'il a ferme la pince ? 
		Delay.msDelay(100);
		System.out.print("la pince est ferme");
		a.fermerLaPince();
		////Commentaire, est-ce qu'il n'a rien fait ?
	}
	public static void main(String[]args) {
		TestActionneurs t = new TestActionneurs();
		//t.testAvancer();
		t.testTourner();
		//t.testReculer();
		//t.testOuvrirLaPince();
		//t.testOuvrirLaPince();
	}

}
